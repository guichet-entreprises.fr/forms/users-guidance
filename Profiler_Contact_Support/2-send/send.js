//--------------- prepare info  before send mail -------------------------

var display = nash.instance.load("display.xml");

var subject = "Formulaire de contact GE : " + _INPUT_.form.contact.subject;
log.info('=> subject : ' + subject);
var sender = _INPUT_.form.contact.email;
log.info('=> sender : ' + sender);
var content = "<html> from : " + sender + "<BR><BR>" + _INPUT_.form.contact.content.replace(/\n/g, "<br>") + "</html>";
log.info('=> content : ' + content);
var receiver = "arnaud.boidard@gmail.com";
log.info('=> receiver : ' + receiver);
// the attachment

var newFileName;
var file;
if(_INPUT_.form.contact.attachment != null && _INPUT_.form.contact.attachment.length > 0){
	var attachment = _INPUT_.form.contact.attachment[0].getLabel();
	log.info('=> attachment : ' + attachment);
	// the attachment path
	var filePath = '/1-input/generated/form.contact.attachment-1-postprocess.pdf';
	log.info('=> filePath : ' + filePath);
	var name = attachment ? attachment.substring(0, attachment.lastIndexOf(".")) : undefined;
	newFileName = name + ".pdf";
	file = nash.util.resourceFromPath(filePath)
	log.info('=> typeof file : ' + typeof file);
	log.info('=> file : ' + file);
}

// call exchange service to send mail
try {
	var target = "${exchange.baseUrl}/email/" + ((newFileName != null) ? "send" : "sendWithoutAttachement");
	
	log.info('=> exchange target : ' + target);
	var response = nash.service.request(target)
	.connectionTimeout(2000)
	.receiveTimeout(5000)
	.param("sender", sender)
	.param("recipient", receiver)
	.param("object", subject)
	.param("content", content)
	if(newFileName != null){
		log.info('=> Calling Exchange with attachment...');
		response = response.param("attachmentPDFName", newFileName)
		.dataType("form")
		.post({"file" : file});
	}else{
		log.info('=> Calling Exchange without attachment...');
		response = response.dataType("application/json") //
		.accept("json")
		.post("");
	}

	//Show success message if response is 200
	if(response.status == 200){

		display.bind("form.result", {
			"result": "Votre message à bien été envoyé au support Guichet Entreprises, vous recevez une réponse en deux jours ouvrés en moyenne."
		});
		return;
	}
	//show message with the potential error and block the step
	else{
		log.error('=> Status error occured while calling Exchange : ' + response.status);
	}
}catch(error) {
	log.error('=> Error occured while calling Exchange : ' + error);
}

display.bind("form.result", {
	"result": "Une erreur technique est survenue lors de l\'envoi de votre demande. Vous pouvez contacter " +
	 "le support technique directement à l'adresse suivante : support@guichet-entreprises.fr. "
});
