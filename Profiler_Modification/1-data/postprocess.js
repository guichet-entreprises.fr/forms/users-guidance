//Redirect user to its approach 

var secteur = $modification.action.choices.secteur.getId();
log.info('Approach selected by user : {}', secteur);

var targets = {
    'commercialArtisanal' : {
        'url' : {
        	'public' : _CONFIG_.get('nash.web.public.url'),
        	'private' : '${nash.ws.private.url}'
        },
        'reference' : "Formalités SCN/ENT/Modification/Déclaration de modification d'activité commerciale et/ou artisanale"
    },
    'liberal' :  {
        'url' : {
        	'public' : _CONFIG_.get('application.public.url'),
        	'private' : '${forms.profiler.private.url}'
        },
        'reference' : "Profiler/Routage/Micro-Entrepreneur"
    },
    'agricultural' : {
        'url' : {
        	'public' : _CONFIG_.get('nash.web.public.url'),
        	'private' : '${nash.ws.private.url}'
        },
        'reference' : "Formalités SCN/ENT/Modification/Déclaration de modification d'activité agricole"
    },
    'agentCommercial' : {
        'url' : {
        	'public' : _CONFIG_.get('nash.web.public.url'),
        	'private' : '${nash.ws.private.url}'
        },
        'reference' : "Formalités SCN/ENT/Modification/Déclaration de modification d'activité d'un agent commercial"
    }
};

var reference = targets[secteur]['reference'];
log.info('Reference target selected : {}', reference);
var target = targets[secteur]['url']['public'].concat('/record/') //
.concat(
    nash.instance.from(targets[secteur]['url']['private']) //
        .createRecord('ref:' + reference) //
        .setAuthor(nash.record.description().author)
        .meta([
            {
                'name':'typeFormality',
                'value': 'modification'
            },
            {
                'name':'uid',
                'value': nash.record.description().recordUid
            }
         ]) //
         .role([
             {
                 "entity" : nash.record.description().author,
                 "role" : '*'
             }
        ]) //
        .save() //
);

log.info('Redirecting user to {}', target);

return { url: target };