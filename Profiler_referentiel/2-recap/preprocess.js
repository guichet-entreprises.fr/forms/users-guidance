isLogging = false;

function fillScreen(groupTarget, value, force) {
	if(isLogging || force){
		data.bind(groupTarget, value);
	}
}

function getAuthorityListFromCfeType(choiceCfe) {
	try {
		authorities = directory.getAuthorityListFromCfeType("GE/" + choiceCfe, debug);
	} catch (e) {
		_log.error("error while calling directory : " + e);
		fillScreen("service.output", {response : e});
		return;
	} finally {
		fillScreen("service.output", { debug: debug.url + debug.status });
	}
	
	fillScreen("service.output", { debug : authorities });

	var result = "CODE EDI; ID GE; LABEL\n";
	authorities['content'].forEach(function(authority) {
		result += authority.details.ediCode;
		result += ";";
		result += authority.entityId;
		result += ";";
		result += authority.label;
		result += "\n";
	});
	return result;
}

function getAuthorityCommuneListFromCfeType(choiceCfe) {
	try {
		authorities = directory.getAuthorityListFromCfeType("GE/" + choiceCfe, debug);
	} catch (e) {
		_log.error("error while calling directory : " + e);
		fillScreen("service.output", { response: e });
		return;
	} finally {
		fillScreen("service.output", { url: debug.url, status: debug.status });
	}
	
	fillScreen("service.output", { debug : authorities });

	var result = "CODE EDI; ID GE; LABEL; CODE COMMUNE; NOM COMMUNE\n";
	authorities['content'].forEach(function(authority) {
		
		var listCommunes = directory.getAuthorityCommuneListFromIdAutority(choiceCfe, authority.entityId);
		listCommunes['content'].forEach(function(commune) {
			result += authority.details.ediCode;
			result += ";";
			result += authority.entityId;
			result += ";";
			result += authority.label;
			result += ";";
			result += commune.details.codeCommune;
			result += ";";
			result += commune.label;
			result += "\n";
		});
	});
	return result;
}

function getFrais() {
	try {
		referentiel = directory.getReferentiel("frais", debug);
	} catch (e) {
		_log.error("error while calling directory : " + e);
		fillScreen("service.output", { response: e });
		return;
	} finally {
		fillScreen("service.output", { url: debug.url, status: debug.status });
	}
	
	fillScreen("service.output", {debug : referentiel});

	var result = "ID; LABEL; PRIX; RESEAU\n";
	referentiel['content'].forEach(function(frai) {
		result += frai.entityId;
		result += ";";
		result += frai.label;
		result += ";";
		result += frai.details.prix;
		result += ";";
		result += frai.details.reseau;
		result += "\n";
	});
	return result;
}


var directory = require('/js/directory.js');

var choiceType = _INPUT_.pageRef.ref.choiceType;
var choiceCfe = _INPUT_.pageRef.ref.choiceCfe;

var data = nash.instance.load("display.xml");

fillScreen("service.output", { request : choiceType + choiceCfe });

var debug = {};
var ref;

if("partenaires" == choiceType.getId()){
	ref = getAuthorityListFromCfeType(choiceCfe);
}
if("partenairesCommune" == choiceType.getId()){
	ref = getAuthorityCommuneListFromCfeType(choiceCfe);
}
if("frais" == choiceType.getId()){
	ref = getFrais();
}

fillScreen("service.output", {response: ref}, true);
