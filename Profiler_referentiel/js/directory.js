/*-------------------------------------------------------------------------------------------+
|    _____       _      _          _     _____           _                   _               
|   / ____|     (_)    | |        | |   |  __ \         | |                 (_)              
|  | |  __ _   _ _  ___| |__   ___| |_  | |__) |_ _ _ __| |_ ___ _ __   __ _ _ _ __ ___  ___ 
|  | | |_ | | | | |/ __| '_ \ / _ \ __| |  ___/ _` | '__| __/ _ \ '_ \ / _` | | '__/ _ \/ __|
|  | |__| | |_| | | (__| | | |  __/ |_  | |  | (_| | |  | ||  __/ | | | (_| | | | |  __/\__ \
|   \_____|\__,_|_|\___|_| |_|\___|\__| |_|   \__,_|_|   \__\___|_| |_|\__,_|_|_|  \___||___/
| 
| 
+--------------------------------------------------------------------------------------------+
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################*/

(function () {

	return {

        /**
         * transform parameter from cardinalite widget to directory format
         * 
         * @param {string} xxxx lorem ipsum
         * @return lorem ipsum
         */
		transformParameterFromCardinalite: function (params) {
			var result = {};
			for (var item in params) {
				if (params[item].id) {
					result[params[item].id] = params[item].value;
				}
			}
			return result;
		},

        /**
         * transform parameter to fill in the cardinalite widget
         * 
         * @param {string} xxxx lorem ipsum
         * @return lorem ipsum
         */
		transformParameterToCardinalite: function (params) {
			var result = [];
			for (var item in params) {
				result.push({ "id": item, "value": params[item], "separator": "<p style=\"text-align:center;margin-top:50%\">🢂</p>" });
			}
			return result;
		},

        /**
         * get the authority's parameters
         * 
         * @param {string} authorityId the authority's id
         * @param {object} debug the debug information
         * @return the authority's parameters
         */
		getAuthorityParameters: function (authorityId, authorityDetails, debug) {
			if (debug === undefined) {
				debug = {};
			}
			if (authorityDetails === undefined) {
				authorityDetails = {};
			}

			var url = "${directory.baseUrl}/v1/authority/{entityId}";
			debug.url = url;
			var searchResult;
			try {
				searchResult = nash.service.request(url, authorityId)
					.connectionTimeout(10000)
					.receiveTimeout(10000)
					.accept('json')
					.get();
			} catch (e) {
				_log.error("error while searching authority on directory : {} ", e);
				throw e;
			} finally {
				debug.status = searchResult.status;
			}

			var response = searchResult.asObject();
			_log.info("response is {}", response);

			var parameters = !response.details.parameters ? null : response.details.parameters;
			_log.info("parameters is {}", parameters);
			authorityDetails.label = response.label
			return parameters;
		},


        /**
         * set the authority's parameters
         * 
         * @param {string} authorityId the authority's id
         * @param {dict} parameters authority's parameters
         * @param {object} debug the debug information
         */
		setAuthorityParameters: function (authorityId, parameters, debug) {
			if (debug === undefined) {
				debug = {};
			}

			var urlSearch = "${directory.baseUrl}/v1/authority/{entityId}";
			debug.url = urlSearch;

			try {
				var searchResult = nash.service.request(urlSearch, authorityId)
					.connectionTimeout(10000)
					.receiveTimeout(10000)
					.accept('json')
					.get();
			} catch (e) {
				_log.error("error while searching authority on directory : {} ", e);
				throw e;
			} finally {
				debug.status = searchResult.status;
			}

			var receiverInfo = searchResult.asObject();
			_log.info("receiverInfo  is {}", receiverInfo);

			receiverInfo.details.parameters = parameters;

			var urlMerge = "${directory.baseUrl}/v1/authority/merge";
			debug.url += " | " + urlMerge;

			try {
				var response = nash.service.request(urlMerge)
					.connectionTimeout(10000)
					.receiveTimeout(10000)
					.dataType('application/json')
					.accept('json')
					.put(receiverInfo);
			} catch (e) {
				_log.error("error while calling directory : " + e);
				throw e;
			} finally {
				debug.status += " | " + response.status;
			}
			debug.authorityLabel = receiverInfo.label;
			return receiverInfo.details.parameters;
		},
		
		/**
         * get the authorities list 
         * 
         * @param {string} cfeType the type of the cfe CMA, URSSAF, etc.
         * @param {object} debug the debug information
         * @return the authorities
         */
		getAuthorityListFromCfeType: function (cfeType, debug) {
			if (debug === undefined) {
				debug = {};
			}

			var url = "${directory.baseUrl}/v1/authority";
			debug.url = url;
			var searchResult;
			try {
				searchResult = nash.service.request(url)
					.connectionTimeout(10000)
					.receiveTimeout(10000)
					.accept('json')
					.param('filters', "details.parent:" + cfeType)
					.param('maxResults', "1000")
					.get();
			} catch (e) {
				_log.error("error while searching authority on directory : {} ", e);
				throw e;
			} finally {
				debug.status = searchResult.status;
			}

			var response = searchResult.asObject();
			return response;
		},
				
		/**
         * get the authorities list 
         * 
         * @param {string} cfeType the type of the cfe CMA, URSSAF, etc.
         * @param {string} idAutority the id of the , URSSAF, etc.
         * @param {object} debug the debug information
         * @return the authorities
         */
		getAuthorityCommuneListFromIdAutority: function (cfeType, idAutority, debug) {
			if (debug === undefined) {
				debug = {};
			}

			var url = "${directory.baseUrl}/v1/authority";
			debug.url = url;
			var searchResult;
			try {
				searchResult = nash.service.request(url)
					.connectionTimeout(10000)
					.receiveTimeout(10000)
					.accept('json')
					.param('filters', "details.parent:ZONES")
					.param('filters', "details." + cfeType + ":" + idAutority)
					.param('maxResults', "5000")
					.get();
			} catch (e) {
				_log.error("error while searching authority on directory : {} ", e);
				throw e;
			} finally {
				debug.status = searchResult.status;
			}

			var response = searchResult.asObject();
			return response;
		},
		
				/**
         * get the authorities list 
         * 
         * @param {string} cfeType the type of the cfe CMA, URSSAF, etc.
         * @param {object} debug the debug information
         * @return the referentiel
         */
		getReferentiel: function (ref, debug) {
			if (debug === undefined) {
				debug = {};
			}

			var url = "${directory.baseUrl}/private/v1/repository/{repositoryId}";
			debug.url = url;
			var searchResult;
			try {
				searchResult = nash.service.request(url, ref)
					.connectionTimeout(10000)
					.receiveTimeout(10000)
					.accept('json')
					.param('maxResults', "1000")
					.get();
			} catch (e) {
				_log.error("error while searching ref on directory : {} ", e);
				throw e;
			} finally {
				debug.status = searchResult.status;
			}

			var response = searchResult.asObject();
			return response;
		}

	};

})();
