//Redirect user to its approach 

var microEntrepreneur = $exercice.action.choices.microEntrepreneur.getId();
log.info('Approach selected by user : {}', microEntrepreneur);

//-->Searching for metadata called 'typeFormality'
var typeFormality = undefined;
for (var i = 0; i < nash.record.meta().metas.size(); i++) {
    if (nash.record.meta().metas.get(i).name == 'typeFormality') {
        typeFormality = nash.record.meta().metas.get(i).value;
        break;
    }
}

log.info('Type formality selected : {}', typeFormality);

var targets = {
    'creation' : {
    	'oui' : "Formalités SCN/ENT/Cessation/Déclaration de cessation d'activité commerciale et/ou artisanale",
    	'non' : "Formalités SCN/ENT/Création/Déclaration de début d'activité libérale/Entreprise individuelle"
    },
    'cessation' :  {
    	'oui' : "Formalités SCN/ENT/Cessation/Déclaration de cessation d'activité d'une entreprise libérale/Micro-entrepreneur",
    	'non' : "Formalités SCN/ENT/Cessation/Déclaration de cessation d'activité libérale hors micro-entrepreneur"
    },
    'modification' :  {
    	'oui' : "Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise libérale/Micro-entrepreneur",
    	'non' : "Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise libérale/Entreprise individuelle"
    }
};

var reference = targets[typeFormality][microEntrepreneur];
log.info('Selecting target : {}', reference);

var target = _CONFIG_.get('nash.web.public.url').concat('/record/') //
.concat(
    nash.instance.from('${nash.ws.private.url}') //
        .createRecord('ref:' + reference) //
        .setAuthor(nash.record.description().author) //
        .meta([
            {
                'name':'uid',
                'value': nash.record.description().recordUid
            }
         ]) //
         .role([
             {
                 "entity" : nash.record.description().author,
                 "role" : '*'
             }
         ]) //
        .save() //
);

log.info('Redirecting user to {}', target);

//nash.record.stepsMgr().requestDone(nash.record.currentStepPosition());

//log.info('Update step status to done and next redirecting user');

return { url: target };