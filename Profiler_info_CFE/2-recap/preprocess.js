function getStringAuthority(authority, libelleIntro){
	_log.info("authority is  {}", authority);
	var result = {}
	
	result["intro"] = libelleIntro + ", voici les coordonnées de votre centre de formalités des entreprises : " + authority.label ;
	
	result["details"] = [];
	if(authority.details.profile.url != null && authority.details.profile.url != ""){
		url = "";
		if(!authority.details.profile.url.startsWith('http://')){
			url = "http://" + authority.details.profile.url;
		}else{
			url = authority.details.profile.url;
		}
		result["details"].push("\nSite internet : <" + url + ">");
	}	
	if(authority.details.profile.email != null && authority.details.profile.email != ""){
		result["details"].push("\nEmail : <" + authority.details.profile.email + ">");
	}
	if(authority.details.profile.tel != null && authority.details.profile.tel != ""){
		result["details"].push("\nTel : " + authority.details.profile.tel);
	}
	if(authority.details.profile.fax != null && authority.details.profile.fax != ""){
		result["details"].push("\nFax : " + authority.details.profile.fax);
	}


	return result;
}

var codeCommune = null;

if(_record.formInfoCfe.pageIntro.info.infoCodePostal.codeCommune != null){
	codeCommune = _record.formInfoCfe.pageIntro.info.infoCodePostal.codeCommune.getId();
}else{
	codeCommune = _record.formInfoCfe.pageIntro.info.infoCommune.libelleCommune.getId();
}

var response = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', codeCommune)
					.accept('json')
					.get();
			
			
var responseObject = response.asObject();
_log.info("codeCommune is  {}", codeCommune);
_log.info("response is  {}", responseObject);
_log.info("details are  {}", responseObject.details);

var listCfe = [];
//GREFFE
var greffe = responseObject.details.GREFFE;
var responseGreffe = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', greffe)
					.accept('json')
					.get();
					
var greffeString =  getStringAuthority(responseGreffe.asObject(), "Vous êtes agent commercial ou en societé civile ou d'exercice libéral");
listCfe.push(greffeString);
//CCI
var cci = responseObject.details.CCI;
var responseCci = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', cci)
					.accept('json')
					.get();
					
var cciString =  getStringAuthority(responseCci.asObject(), "Vous êtes pur commercant");
listCfe.push(cciString);
//CMA
var cma = responseObject.details.CMA;
var responseCma = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', cma)
					.accept('json')
					.get();
					
var cmaString = getStringAuthority(responseCma.asObject(), "Vous êtes artisan");
listCfe.push(cmaString);
//CA
var ca = responseObject.details.CA;
var responseCa = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', ca)
					.accept('json')
					.get();
					
var caString = getStringAuthority(responseCa.asObject(), "Vous êtes agriculteur ou exercez une activité d'élevage");
listCfe.push(caString);
//URSSAF
var urssaf = responseObject.details.URSSAF;
var responseUrssaf = nash.service.request('${directory.baseUrl}/v1/authority/{authorityId}', urssaf)
					.accept('json')
					.get();
					
var urssafString = getStringAuthority(responseUrssaf.asObject(), "Vous êtes libéral en entreprise individuelle");
listCfe.push(urssafString);

_log.info("listCfe {}", listCfe);

var groups = [];
var dataCfe = [];

for(e in listCfe){
	dataCfe.push(spec.createGroup({
					id: "CFE" + e,
					fold : 'yes',
					label :  listCfe[e].intro,
					mandatory : false,
					description : listCfe[e].details.join(" | ")
				}));
}

groups.push(spec.createGroup({
			id : 'listeCfe',
			fold : 'no',
			label : "liste de vos centres de formalités des entreprises",
			data : dataCfe
		}));

return spec.create({
	id: 'result',
	label: "Liste des CFE (" + responseObject.label +  ")",
	groups: [spec.createGroup({
			id : 'listeCfe',
			fold : 'yes',
			label : "Voici la liste de vos centres de formalités des entreprises",
			data : groups
	})]
});
