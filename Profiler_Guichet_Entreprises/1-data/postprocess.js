//-->Get last message from quota reference
function tracker(reference) {
    var messages = [];
	try {
		var response = nash.service.request('${tracker.baseUrl}/v1/uid/' + reference) //
	        .connectionTimeout(10000) //
	        .receiveTimeout(10000) //
	        .dataType('application/json')//
	        .accept('json') //
	        .get();
	    if (null == response || response.status != 200 || null == response.asObject()['messages']) {
	        return messages;
	    }
	    messages = response.asObject()['messages'];
		log.info("Tracker messages for reference '{}' : {}", reference, messages);
	    return messages;
	} catch (error) {
		log.error("Unable to get Tracker messages for reference " + reference);
        return [];
	}
    return messages;
}
//<--

var typeFormality = $context.action.typeFormality.choice.getId();
log.info('Formality type by user : {}', typeFormality);

var targets = {
    'agricultural' : {
        'typeFormality' : 'creation',
        'url' : {
            'public' : _CONFIG_.get('nash.web.public.url'),
            'private' : '${nash.ws.private.url}'
        },
        'reference' : "Formalités SCN/ENT/Création/Déclaration de début d'activité agricole/Entreprise individuelle",
        'secteur' : undefined
    },
    'cessation' :  {
        'typeFormality' : 'cessation',
        'url' : {
            'public' : _CONFIG_.get('forms.company.cessation.public.url'),
            'private' : undefined
        },
        'reference' : undefined,
        'secteur' : undefined,
        'default' : _CONFIG_.get('forms.company.cessation.public.url')
    },
    'creation' : {
        'typeFormality' : 'creation',
        'url' : {
            'public' : _CONFIG_.get('forms.company.creation.public.url'),
            'private' : undefined
        },
        'reference' : undefined,
        'secteur' : undefined,
        'default' : _CONFIG_.get('forms.company.creation.public.url')
    },
    'regularization' : {
        'typeFormality' : 'regularization',
        'url' : {
            'public' : _CONFIG_.get('forms.company.regularization.public.url'),
            'private' : undefined
        },
        'reference' : undefined,
        'secteur' : undefined,
        'default' : _CONFIG_.get('forms.company.regularization.public.url')
    },
    'welcome' : {
        'typeFormality' : undefined,
        'url' : {
            'public' : _CONFIG_.get('welcome.public.url'),
            'private' : undefined
        },
        'reference' : undefined,
        'secteur' : undefined,
        'default' : _CONFIG_.get('welcome.public.url')
    },
    'modification' : {
        'typeFormality' : 'modification',
        'url' : {
            'public' : _CONFIG_.get('nash.web.public.url'),
            'private' : '${nash.ws.private.url}'
        },
        'reference' : undefined,
        'secteur' : {
            'commercialArtisanal' : "Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise commerciale et/ou artisanale",
            'liberal' :  {
                'micro' : {
                    'oui' : "Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise libérale/Micro-entrepreneur",
                    'non' : "Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise libérale/Entreprise individuelle"
                }
            },
            'modifAgricultural' : "Formalités SCN/ENT/Modification/Déclaration de modification d'une entreprise agricole ou d'une activité de bailleur de biens ruraux",
            'agentCommercial' : "Formalités SCN/ENT/Modification/Déclaration de modification d'un agent commercial"
        },
        'default' : _CONFIG_.get('forms.company.modification.public.url')
    }
};

var target = targets[typeFormality]['url']['public'];
if ('modification' !== typeFormality && 'agricultural' !== typeFormality) {
	//-->Redirection for creation, cessation, regularization and welcome
	log.info('Redirecting user to {}', target);
	return { url: target };
}

var secteur = null != $context.action.secteur.choice ? $context.action.secteur.choice.getId() : undefined;
if (undefined == secteur) {
	var reference = targets[typeFormality]['reference'];
} else {
	if (undefined == targets[typeFormality]['secteur']) {
		var reference = targets[typeFormality]['reference'];
	} else {
		var microEntrepreneur = null != $context.action.microEntrepreneur.choice ? $context.action.microEntrepreneur.choice.getId() : undefined;
		if (undefined == microEntrepreneur) {
			var reference = targets[typeFormality]['secteur'][secteur];
		} else {
			var reference = targets[typeFormality]['secteur'][secteur]['micro'][microEntrepreneur];
		}
	}
}
log.info('Reference found {}', reference);

//-->Get uid formality model
var request = nash.service.request('${nash.ws.private.url}/v1/Specification/ref/' + encodeURI(reference) + '/info') //
    .connectionTimeout(10000) //
    .receiveTimeout(10000) //
    .dataType('application/json')//
    .accept('json') //
    .get();
var specification = request.asObject();
//<--

//-->Building Tracker JSON to get total number of open records and the quota authorized
var trackerReferences = [
  'GE-QUOTA-' + specification.code,
  'GE-OPEN-' + specification.code
];

var trackerMessages = {};
trackerReferences.forEach(function (reference) {
	trackerMessages[reference] = tracker(reference);
});

//-->Get the latest message for reference using quota
var messages = trackerMessages[trackerReferences[0]];

var quota = messages.length == 0 ? -1 : messages[messages.length - 1]['content'];
if (isNaN(parseInt(quota))) {
  quota = -1;
} else {
	quota = parseInt(quota);
}
log.info("Quota authorized for the specification '{}' tagged '{}' : {}", specification.title, specification.code, quota);

if (-1 == quota || trackerMessages[trackerReferences[1]].length < quota) {
	target = target.concat('/record/') //
	    .concat(
	        nash.instance.from(targets[typeFormality]['url']['private']) //
	            .createRecord('ref:' + reference) //
	            .setAuthor(nash.record.description().author) //
	            .meta([
	                {
	                    'name':'typeFormality',
	                    'value': targets[typeFormality]['typeFormality']
	                },
	                {
	                    'name':'uid',
	                    'value': nash.record.description().recordUid
	                }
	             ]) //
	             .role([
	                {
	                    "entity" : nash.record.description().author,
	                    "role" : '*'
	                }
	             ]) //
	            .save() //
	);
} else {
	target = targets[typeFormality]['default'];
}
log.info('Redirecting user to {}', target);

return { url: target };